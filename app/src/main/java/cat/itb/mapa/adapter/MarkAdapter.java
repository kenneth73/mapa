package cat.itb.mapa.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import cat.itb.mapa.MainActivity;
import cat.itb.mapa.R;
import cat.itb.mapa.fragment.FragmentRecycler;
import cat.itb.mapa.fragment.MapsFragment;
import cat.itb.mapa.model.Marcador;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import static com.google.android.material.snackbar.Snackbar.make;

public class MarkAdapter extends FirebaseRecyclerAdapter<Marcador, MarkAdapter.MarkHolder> {

    private final Context context;
    public View view; // para poder utilizar view en la funcion showUndoSnackbar()
    public static int positionToDelete;

    public MarkAdapter(@NonNull FirebaseRecyclerOptions<Marcador> options, Context context) {super(options);
        this.context = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull MarkAdapter.MarkHolder holder, int position, @NonNull Marcador model) {
        holder.bind(model);
        //this.comic = model;
    }

    @NonNull
    @Override
    public MarkHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_item_mark, parent, false);
        this.view = v;
        return new MarkHolder(v);
    }



    public class MarkHolder extends RecyclerView.ViewHolder{
        ImageView imageViewMark;
        TextView textViewTitle;
        TextView textViewType;
        TextView textViewScore2;

        public MarkHolder(@NonNull View itemView) {
            super(itemView);
            imageViewMark = itemView.findViewById(R.id.imageViewMark);
            textViewTitle = itemView.findViewById(R.id.textViewTitle2);
            textViewType = itemView.findViewById(R.id.textViewType2);
            textViewScore2= itemView.findViewById(R.id.textViewScore2);
        }

        public void bind(final Marcador model) {
            if (model.getUrlImg().equals(" ")) {
                model.setUrlImg("https://www.google.com/url?sa=i&url=https%3A%2F%2Fstackoverflow.com%2Fquestions%2F37897301%2Fput-default-image-in-acf&psig=AOvVaw0t5skh8fc4JO7INqSk8TAf&ust=1614111707306000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKC3luio_u4CFQAAAAAdAAAAABAN");
            }
            try {
                Picasso.with(imageViewMark.getContext())
                        .load(model.getUrlImg())
                        .into(imageViewMark);
            } catch (Exception e) {
                Log.e("MarkAdapter", e.toString());
            }

            textViewTitle.setText("Title: "+model.getTitle());
            textViewType.setText("Type: "+model.getType());

            textViewScore2.setText("Score: "+ model.getScore() + "/5");


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    MainActivity mainActivity = (MainActivity)context;

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("marcador", model);

                    MapsFragment mapsFragment = new MapsFragment();

                    mapsFragment.setArguments(bundle);

                    changeFragment(mapsFragment);
                    try {
                        mainActivity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mapsFragment).commit();
                    }catch (Exception e){
                        Log.e("errorNAvigation", "ERROR AL NAVEGAR ENTRE FRAGMENTS");
                    }
                }
            });
        }
    }



    public void deleteItem(int position) {
        positionToDelete=position;
        Marcador m = getItem(positionToDelete);
        FragmentRecycler.apiFirebase.delete(m);
        notifyItemRemoved(positionToDelete);
        Log.e("delete", "delete item nº: " + position);
    }


    public Context getContext() {
        return context;
    }

    private void changeFragment(Fragment currentFragment) {
        Log.e("changeFrag", "asdasda");
        FragmentManager manager = ((AppCompatActivity)getContext()).getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.fragment_container, currentFragment).commit();
    }
}
