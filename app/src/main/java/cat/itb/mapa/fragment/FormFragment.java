package cat.itb.mapa.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import cat.itb.mapa.R;
import cat.itb.mapa.firebase.ApiFirebase;
import cat.itb.mapa.model.Marcador;
import androidx.navigation.Navigation;

import static android.app.Activity.RESULT_OK;


public class FormFragment extends Fragment implements View.OnClickListener {
    static final int REQUEST_IMAGE_CAPTURE = 100;
    File fileUrl;

    private Button btnSave, btnTakePhoto;
    private TextInputLayout titleLayout;
    private TextInputEditText titleInput;
    private TextView textViewErrors;
    private ImageView imageView;
    private Spinner spinnerType;
    private RatingBar ratingBar;
    private ProgressBar progressBar;


    private int positionRatingBar = 0;
    double lat, lon;

    Marcador newMarcador;

    private String[] types_marks_array ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        types_marks_array = getResources().getStringArray(R.array.tpye_mark_array);

        Bundle bundle= getArguments();
        assert bundle != null;
        lat = bundle.getDouble("latitud");
        lon = bundle.getDouble("longitude");
        Log.i("FragmentForm", "onCreate FragmentForm");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_form, container, false);
        btnSave = v.findViewById(R.id.buttonSave);
        btnTakePhoto = v.findViewById(R.id.buttonPutImage);
        btnSave.setOnClickListener(this);
        btnTakePhoto.setOnClickListener(this);
        titleLayout = v.findViewById(R.id.title_mark_layout);
        titleInput = v.findViewById(R.id.title_mark_input);
        spinnerType = v.findViewById(R.id.spinnerTypes);
        imageView = v.findViewById(R.id.imageView);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.tpye_mark_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerType.setAdapter(adapter);

        ratingBar = v.findViewById(R.id.ratingBar);
        ratingBar.setRating(2.5f);

        // = v.findViewById(R.id.progressBar);
        progressBar = new ProgressBar(getContext());
        progressBar.setVisibility(View.INVISIBLE);



        textViewErrors = v.findViewById(R.id.textViewErrors);
        textViewErrors.setVisibility(View.INVISIBLE);


        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionRatingBar= position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getContext(), "stateBookSpinner.onNothingSelected()", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public Boolean validateAndSaveForm() {
        boolean checkFields = true;
        String title= titleInput.getText().toString();
        float score = ratingBar.getRating();
        String type = types_marks_array[positionRatingBar];


        if (title != null &&title.length() < 1) {
            titleLayout.setError("title is required");
            checkFields = false;
        } else {
            titleLayout.setError("");
        }

        if(positionRatingBar == 0) {
            textViewErrors.setError("type is required");
            textViewErrors.setText("type is required");
            textViewErrors.setVisibility(View.VISIBLE);
            checkFields = false;
        } else {
            textViewErrors.setVisibility(View.INVISIBLE);
        }

        if (checkFields) {
            progressBar.setVisibility(View.VISIBLE);
            //guardar datos db y foto  local storage
            Log.i("FragmentRecycler", "guardar datos db y foto local storage ");
            newMarcador = new Marcador("", lat, lon, type, title, "urlImg", score);
            ApiFirebase apiFirebase = new ApiFirebase("maps", getContext());
            //Log.i("fileUrlsdss", fileUrl.getName());
            if (fileUrl != null) {
                apiFirebase.comprimirImagen(getContext(), fileUrl);
                apiFirebase.guardarMarcador(newMarcador);
                FragmentRecycler fragmentRecycler = new FragmentRecycler();
                changeFragment(fragmentRecycler);
            } else {

                textViewErrors.setText("image is required");
                textViewErrors.setVisibility(View.VISIBLE);
            }

        }
        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonPutImage:
                //intent para hacer una foto
                CropImage.startPickImageActivity(getContext(), FormFragment.this);
                Log.d("addimgfragment", "chooseBtnAddImgFragment");
                break;
            case R.id.buttonSave:
                validateAndSaveForm();
                /*FragmentRecycler fragmentRecycler = new FragmentRecycler();
                changeFragment(fragmentRecycler);*/
                break;
        }
    }

    private void changeFragment(Fragment currentFragment) {
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, currentFragment).commit();
    }


    //crop image
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(imageBitmap);
        }
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri= CropImage.getPickImageResultUri(getContext(), data);
            recortarImagen(imageUri);
        }

        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                fileUrl = new File(resultUri.getPath());
                Picasso.with(getContext()).load(fileUrl).into(imageView);
            }
        }
    }

    public void recortarImagen(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 480)
                .setAspectRatio(2, 1).start(getContext(), FormFragment.this);
    }


}