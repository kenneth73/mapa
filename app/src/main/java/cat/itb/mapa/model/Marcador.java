package cat.itb.mapa.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class Marcador implements Serializable {
    public static final String attrId = "id";
    public static final String attrLat = "lat";
    public static final String attrLon = "lon";
    public static final String attrImg = "img";
    public static final String attrRating = "rating";

    private String id;
    private double lat;
    private double lon;
    private String type;
    private String title;
    private String urlImg;
    private float score;

    public Marcador() { }

    public Marcador(String id, double lat, double lon, String type, String title, String urlImg, float score) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.type = type;
        this.title = title;
        this.urlImg = urlImg;
        this.score = score;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    //nombre coger para que no se añada el campo a firebase automaticamente
    public LatLng cojerLatLng() {
        return new LatLng(lat,lon);
    }

}
