package cat.itb.mapa.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import cat.itb.mapa.R;
import cat.itb.mapa.MainActivity;
import cat.itb.mapa.firebase.ApiFirebase;
import cat.itb.mapa.model.Marcador;

public class MapsFragment extends Fragment implements OnMapReadyCallback{
    private static final int DEFAULT_ZOOM = 15;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean locationPermissionGranted;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location lastKnownLocation;
    private final LatLng defaultLocation = new LatLng(-33.8523341, 151.2106085);

    View rootView;
    MapView mapView;
    GoogleMap googleMap;

    public static ApiFirebase apiFirebase;
    ArrayList<Marcador> marcadorArrayList = new ArrayList<>();

    private FloatingActionButton fab;

    private Location locationActual;
    Marcador marcadorDelOnclick;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_maps, container, false);


        fab = rootView.findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Log.i("fab.setOnClick", "navegar hasta el formulario(pasandole las coordenadas");

                Bundle bundle = new Bundle();
                bundle.putDouble("latitud", locationActual.getLatitude());
                bundle.putDouble("longitude", locationActual.getLongitude());


                FormFragment fragmentForm = new FormFragment();
                fragmentForm.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragmentForm).commit();

            }
        });

        Bundle bundle= getArguments();
        assert bundle != null;

        if (bundle.get("marcador")!= null) {
            marcadorDelOnclick = (Marcador) bundle.get("marcador");
            Log.i("FragmentForm", marcadorDelOnclick.getTitle());
        }


        return  rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = rootView.findViewById(R.id.map); //cargamos la vista
        if (mapView != null) { //llamamos al onCreate etc porque es una vista
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this); //carga el mapa
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
            Log.e("mapa", "onViewCreated en el mapa!");

        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);//opcional

        this.googleMap.setMinZoomPreference(15);//del 1 al 21
        this.googleMap.setMinZoomPreference(18);
        //LatLng itb =  new LatLng(41.4531987,2.1843371);
        //LatLng casa =  new LatLng(41.4576343,2.2441232);

        apiFirebase = new ApiFirebase("maps", MainActivity.contextActivity);
        apiFirebase.databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Marcador m = postSnapshot.getValue(Marcador.class);
                    Log.e("addValueEventListener",m.getTitle());
                    marcadorArrayList.add(m);
                    addMarkerInMaps(m.cojerLatLng(), m.getTitle(), m.getType());
                    Log.e("latitudes", (m.getLat())+ " " + (m.getLon()));
                }

                for (Marcador m : marcadorArrayList) {
                    addMarkerInMaps(m.cojerLatLng(), m.getTitle(), m.getType());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        if (marcadorDelOnclick == null) {//si marcador es null, cogemos la posicion actual para mostrar en el mapa
            getLocationPermission();
            updateLocationUI();
            getDeviceLocation();
        } else {//si marcador no es null, cogemos la posicion del marcador para mostrar en el mapa

            LatLng casa =  new LatLng(marcadorDelOnclick.getLat(),marcadorDelOnclick.getLon());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(casa)
                    .zoom(DEFAULT_ZOOM)
                    .bearing(0f)
                    .tilt(30)
                    .build();
            this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }


        //posiciona el mapa/camara en la posicion 'casa'
        /*CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(casa)
                .zoom(DEFAULT_ZOOM)
                .bearing(0f)
                .tilt(30)
                .build();
        this.googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/



        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                Log.e("longClickMap", "latLng.latitude: "+latLng.latitude+"latLng.longitude: "+latLng.longitude);

                //pasar parametros al fragment
                Bundle bundle = new Bundle();
                bundle.putDouble("latitud", latLng.latitude);
                bundle.putDouble("longitude", latLng.longitude);

                FormFragment fragmentForm = new FormFragment();
                fragmentForm.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragmentForm).commit();

                //addMarkerInMaps(latLng, "asd", "asd");

            }
        });

        /*googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Toast.makeText(getContext(), "latLng.latitude: "+marker.getPosition()+marker.getTitle(), Toast.LENGTH_LONG).show();
                return true;

            }
        });*/

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                //Toast.makeText(getContext(), "latLng.latitude: "+marker.getPosition()+marker.getTitle(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                //Toast.makeText(getContext(), "latLng.latitude: "+marker.getPosition()+marker.getTitle(), Toast.LENGTH_LONG).show();

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                //actualizar el marker
                Log.e("dragMArker", marker.getId());

            }
        });

    }


    public void addMarkerInMaps(LatLng latLng, String title, String snippet) {

        //crear markers
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(title);
        markerOptions.snippet(snippet);
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(android.R.drawable.alert_dark_frame));
        markerOptions.draggable(true);
        //dentro del mapa crear interfaz con un lisener para cambiar ...//onStopDrag, cojer la latitud long ...
        googleMap.addMarker(markerOptions);
        Log.e("tag", " mark added");
    }


    //coge la posicion actual
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {

                            // Set the map's camera position to the current location of the device.
                            lastKnownLocation = task.getResult();
                            if (lastKnownLocation != null) {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(lastKnownLocation.getLatitude(),
                                                lastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                Log.e("asd", String.valueOf(lastKnownLocation.getLatitude()));
                                Log.e("asd", String.valueOf(lastKnownLocation.getLongitude()));
                                locationActual = lastKnownLocation;

                            }
                        } else {
                            Log.d("TAGG", "Current location is null. Using defaults.");
                            Log.e("TAGG", "Exception: %s", task.getException());
                            googleMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(defaultLocation, DEFAULT_ZOOM));
                            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage(), e);
        }
    }

    //comprueba los permisos de localizacion
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getContext(),//this.getApplicationContext()
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    //comprueba los permisos de localizacion
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    //actualiza la localizacion
    private void updateLocationUI() {
        if (googleMap == null) {
            return;
        }
        try {
            if (locationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                lastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }
}