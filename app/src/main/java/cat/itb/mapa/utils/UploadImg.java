package cat.itb.mapa.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.zelory.compressor.Compressor;

public class UploadImg {
    DatabaseReference databaseReference;
    StorageReference storageReference;
    private final Context context;
    Fragment fragment;

    Bitmap thumbBitmap;
    String nameImg;
    byte[] thumbByte;
    File fileUrl;

    public UploadImg(Context context, Fragment fragment) {
        this.context = context;
        this.fragment = fragment;
        databaseReference= FirebaseDatabase.getInstance().getReference().child("mapa");//mapa nombre del contenedor de la aplicacion
        storageReference = FirebaseStorage.getInstance().getReference().child("img"); //carpeta del storage
        Log.i("FragmentRecycler", "UploadImg constructor OKEY");
    }





    public void comprimirImagen(Context context) {
        try {
            thumbBitmap = new Compressor(context)
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(fileUrl);

        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumbByte = byteArrayOutputStream.toByteArray();
    }

    public void subirImg() {
        //progressBar.setVisibility(View.VISIBLE);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        nameImg = timestamp + ".jpg";

        final StorageReference ref = storageReference.child(nameImg);
        StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setCustomMetadata("clave1", "valor1")
                .setCustomMetadata("clave2", "valor2")
                .build();
        UploadTask uploadTask = ref.putBytes(thumbByte, storageMetadata);

        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()){
                    throw (task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri downloadUri = task.getResult();
                databaseReference.push().child("urlfoto").setValue(downloadUri.toString());
                //progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(context, "Imagen subida", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
