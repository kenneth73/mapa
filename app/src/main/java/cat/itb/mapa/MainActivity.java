package cat.itb.mapa;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import cat.itb.mapa.R;
import cat.itb.mapa.firebase.ApiFirebase;
import cat.itb.mapa.fragment.FragmentRecycler;
import cat.itb.mapa.fragment.MapsFragment;
import cat.itb.mapa.model.Marcador;

public class MainActivity extends AppCompatActivity {

    /*fragment actual que mostramos*/
    Fragment currentFragment;
    public static Context contextActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contextActivity = getApplicationContext();
        currentFragment= new FragmentRecycler();
        //currentFragment = new MapsFragment();
        changeFragment(currentFragment);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_list:
                currentFragment= new FragmentRecycler();
                break;

            case R.id.menu_mapa:
                /*le paso null para saber si viene del onClick del menu
                si viene de aqui se cargara la posicion actual ya que le paso null
                si viene de un item click le pasare el marcador(desde la clase MarkAdapter) y se posicionara en el mapa en el marcador clicado*/
                Bundle bundle = new Bundle();
                bundle.putSerializable("marcador", null);
                currentFragment = new MapsFragment();
                currentFragment.setArguments(bundle);
                break;
        }

        changeFragment(currentFragment);
        return super.onOptionsItemSelected(item);
    }

    private void changeFragment(Fragment currentFragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, currentFragment).commit();
    }

}