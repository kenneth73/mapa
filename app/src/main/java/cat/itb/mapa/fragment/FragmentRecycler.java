package cat.itb.mapa.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import cat.itb.mapa.R;
import cat.itb.mapa.MainActivity;
import cat.itb.mapa.adapter.MarkAdapter;
import cat.itb.mapa.firebase.ApiFirebase;
import cat.itb.mapa.utils.SwipeToDeleteCallback;


public class FragmentRecycler extends Fragment {
    public static ApiFirebase apiFirebase;
    RecyclerView recyclerView;
    MarkAdapter markAdapter;

    public FragmentRecycler() { }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recycler_view, container, false);
        recyclerView = v.findViewById(R.id.recyclerView);
        apiFirebase = new ApiFirebase("maps", MainActivity.contextActivity);
        markAdapter = new MarkAdapter(apiFirebase.firebaseRecyclerOptions, getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(markAdapter);
        setUpRecyclerView();
        return v;
    }

    //para el markAdapter firebase
    @Override
    public void onStart() {
        super.onStart();
        markAdapter.startListening();
    }
    //para el markAdapter firebase
    @Override
    public void onStop() {
        super.onStop();
        markAdapter.stopListening();
    }

    //para borrar al deslizar el item
    private void setUpRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.contextActivity));
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(markAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
}