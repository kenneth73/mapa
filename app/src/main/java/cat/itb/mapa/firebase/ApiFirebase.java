package cat.itb.mapa.firebase;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cat.itb.mapa.model.Marcador;
import id.zelory.compressor.Compressor;

public class ApiFirebase {
    public static FirebaseDatabase firebaseDatabase;
    public static DatabaseReference databaseReference;
    public static StorageReference storageReference;
    public static FirebaseRecyclerOptions<Marcador> firebaseRecyclerOptions;

    Bitmap thumbBitmap;
    byte[] thumbByte = null;
    Context context;

    public ApiFirebase(String nameDatabase, Context context) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(nameDatabase);
        firebaseRecyclerOptions = new FirebaseRecyclerOptions.Builder<Marcador>()
                .setQuery(databaseReference, Marcador.class).build();
        storageReference = FirebaseStorage.getInstance().getReference().child("img"); //carpeta del storage
        this.context = context;
    }

    public static void insert(Marcador m) {
        String key = databaseReference.push().getKey();
        assert key != null;
        m.setId(key);
        databaseReference.child(key).setValue(m);

    }

    public static void update(Marcador m) {
        databaseReference.child(m.getId()).setValue(new Marcador(m.getId(), m.getLat(), m.getLon(), m.getType(), m.getTitle(), m.getUrlImg(), m.getScore()));
    }

    public void delete(Marcador m) {
        databaseReference.child(m.getId()).removeValue();
    }


    public void comprimirImagen(Context context, File fileUrl) {
        Log.i("fileUrl", fileUrl.getAbsolutePath());
        if (fileUrl != null) {
            try {
                thumbBitmap = new Compressor(context)
                        .setMaxHeight(125)
                        .setMaxWidth(125)
                        .setQuality(50)
                        .compressToBitmap(fileUrl);

            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
            thumbByte = byteArrayOutputStream.toByteArray();
        }

    }

    //guarda la imagen en storage, despues en database
    public void guardarMarcador(final Marcador m) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        String nameImg = timestamp + ".jpg";

        final StorageReference ref = storageReference.child(nameImg);
        StorageMetadata storageMetadata = new StorageMetadata.Builder().build();
        /*StorageMetadata storageMetadata = new StorageMetadata.Builder()
                .setCustomMetadata("clave1", "valor1")
                .setCustomMetadata("clave2", "valor2")
                .build();*/
        UploadTask uploadTask = ref.putBytes(thumbByte, storageMetadata);

        Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()){
                    throw (task.getException());
                }
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {

                Uri downloadUri = task.getResult();
                m.setUrlImg(downloadUri.toString());
                String key = databaseReference.push().getKey();
                assert key != null;
                m.setId(key);
                databaseReference.child(key).setValue(m);
                Toast.makeText(context, "Imagen subida", Toast.LENGTH_SHORT).show();

            }
        });
    }

}
